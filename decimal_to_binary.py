def decimal_to_binary(number:int) -> str:
    num = number
    binary = ""
    while num != 0:
        if num % 2 == 0:
            num /= 2
            binary += "0"
        else:
            num = num - 1
            num /=2
            binary+= "1"
    return binary[::-1]

number = int(input("Quel nombre voulez vous mettre en binaire ? : "))

print(decimal_to_binary(number))