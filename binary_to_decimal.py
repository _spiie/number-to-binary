def binary_to_decimal(number:str) -> int:
    final = 0
    num = 1
    while len(number) != 0:
        final += int(number[-1])*num
        number = number[:-1]
        num *= 2
    return final

# PROGRAME PRINCIPAL

number = input("Quel nombre binaire voulez vous transformer en nombre ? : ")
setNumber = set(number)
while {"0", "1"} != setNumber and setNumber != {'0'} and setNumber != {'1'}:
    number = input("Le nombre binaire entré est invalide, ré essayez. : ")
    setNumber = set(number)

print(binary_to_decimal(number))
